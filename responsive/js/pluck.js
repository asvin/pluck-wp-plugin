var pluckCustomization = {

	"personaLoaded": false,
	"mobileBreak": 480,
	"tabletBreak": 600,
	"siteMaxWidth": 850,												// max width of full-page widgets 
	"mostHelpfulWidgetWidth": .9, 							// 100% of the .content-container width
	"blogWidgetWidth": 1, 											// 100% of window width up to max-width
	"blogImageDefaultFloat": 'right',						// direction images in the blog should align
	"numberOfStars": 5,													// number of stars available for rating (5)
	"pluckPersonaMenuScrollSpeed": 3, 					// number of menu items to scroll in persona menu

	"customizePersonaWidget":  function(){
		if( this.personaLoaded ) return;
		// move the 'view all' friends link
		var viewAllFriendsSpan = dmJQuery('span.pluck-persona-userbar-avatar-quickguide-action');
		var friendsDiv = dmJQuery('div.pluck-persona-userbar-avatar-quickguide-friends');
		dmJQuery(friendsDiv).append(viewAllFriendsSpan);

		// set up the hamburger menu for mobile
		pluckCustomization.setUpHamburgerMenu();

		// set up the custom scrolling menu
		pluckCustomization.setUpScrollingMenu();

		// add stars to any review in the home feed
		var anchors = dmJQuery('.pluck-persona-update-message-info').children('a');
		var starsHeight = 16; // height of stars png in pixels
		dmJQuery.each( anchors, function( index, anchor ){
			var link = dmJQuery(anchor).attr('href');
			if( link.indexOf('plckReviewKey') != -1 ){
				if(anchor.previousSibling.data.indexOf('Commented') > -1 ) return true;
				var container = anchor.parentElement.parentElement.parentElement;
				var contentPreview = dmJQuery(container).children('.pluck-persona-update-content-preview').children('.pluck-persona-update-content-preview-text');
				dmJQuery(contentPreview).prepend('<div class="pluck-persona-review-stars-custom"></div>');

				// get the plckReviewKey from the link url
				var plckReviewKey = pluckCustomization.parsePluckArgFromUrl( link, 'plckReviewKey' );

				// SDK call to retrieve the review meta data
				var reviewRating;
				var reviewRequest = new PluckSDK.ReviewRequest();
				var reviewKey = new PluckSDK.ReviewKey();
				reviewKey.Key = plckReviewKey;
				reviewRequest.ReviewKey = reviewKey;
				PluckSDK.SendRequests([reviewRequest], function( resp ){
					if( resp[0] && resp[0].ResponseStatus && resp[0].ResponseStatus.StatusCode && resp[0].ResponseStatus.StatusCode == PluckSDK.ResponseStatusCode.OK ){
						// add rating stars
						reviewRating = resp[0].Review.Rating;
						bgTopScrollPosn = reviewRating * starsHeight;
						var starsDiv = dmJQuery(contentPreview).find('.pluck-persona-review-stars-custom');
						dmJQuery(starsDiv).css('background', "url('../images/sprite-flat-stars.png') no-repeat 0 -" + bgTopScrollPosn + "px" );

						// add any photos that were submitted with the review
						var photoKeys = resp[0].Review.PhotoKeys;
						var photoRequests = [];
						for ( j=0; j<photoKeys.length; j++ ){
								var photoRequest = new PluckSDK.PhotoRequest();
							var photoKey = new PluckSDK.PhotoKey();
							photoKey.Key = photoKeys[j].Key;
							photoRequest.PhotoKey = photoKey;
							photoRequests.push(photoRequest);
						}

						if( photoRequests.length > 0 ){
								PluckSDK.SendRequests( photoRequests, function( photoResponse ){
									if ( photoResponse && photoResponse.length > 0 ){
										var divImages = '<div class="pluck-persona-review-photos-custom"></div>';
									dmJQuery(contentPreview).append(divImages);
									for( k=0; k<photoResponse.length; k++ ){
										var photo = photoResponse[k].Photo;
										var blockingState = photo.ContentBlockingState;
										var pendingApproval = photo.IsPendingApproval;
										var photoTitle = photo.Title;
										var image = photo.Image;

										if( image && !pendingApproval && blockingState && blockingState == PluckSDK.ContentBlockingEnum.Unblocked ){
											var url = image.P4Photo;
											var jqDivImages = dmJQuery(contentPreview).children('.pluck-persona-review-photos-custom');

											dmJQuery(jqDivImages).append('<img src="' + url + '" title="' + photoTitle + '" class="pluck-persona-review-photo-custom" />');
										}
									}
								}
							});
						}
					}
				});
			}
		});
		this.personaLoaded = true;
	},

	"parsePluckArgFromUrl": function( url, arg ){
		var returnVal = 'none';
		//separate the url and the arguments
		var arrUrl = url.split('?');
		if ( arrUrl.length > 1 ){
			var arrArgs = arrUrl[1].split('&');
			for( i=0; i<arrArgs.length; i++ ){
				var singleArg = arrArgs[i].split('=');
				if ( singleArg.length > 1 && singleArg[0] == arg ){
					returnVal = singleArg[1];
				}
			}
		}

		return returnVal;
	}, 

	"customizeReviewRollup": function(){
		// move the 2nd dimension ratings  to the dialog
		// so they don't show by default
		var dialogWrapper = dmJQuery("div.pluck-review-rollup-dialog div.pluck-dialog-wrapper");
		var attributeRollups = dmJQuery("div.pluck-review-rollup-attributes-wrap");
		dmJQuery(dialogWrapper).append(attributeRollups);

		// get the average review rating to display in the
		// numberical "out of 5.0 stars" box.
		var reviewOnKey = dmJQuery('.pluck-review-rollup').attr('reviewonkey');
		req = new PluckSDK.ReviewRollupRequest();
		req.ReviewedKey = new PluckSDK.ExternalResourceKey({ 'Key': reviewOnKey });
		PluckSDK.SendRequests([req], function(resp){
			var average = resp[0].ReviewRollup.Rating.toFixed(1);
			var numReviews = resp[0].ReviewRollup.TotalReviews;
			var averageDiv = '<div class="custom-dialog-average-rating">' + average + '</div>';
			dmJQuery('.pluck-dialog-wrapper').prepend(averageDiv);

			// copy the stars from the rollup widget to 
			// display in the dialog
			var copyOfRollup = dmJQuery('.pluck-review-rollup-wrapper').clone();
			var rollupAttributes = dmJQuery('.pluck-review-rollup-attributes-wrap');
			var dialogWrapper = dmJQuery('.pluck-dialog-wrapper');
			var numReviewsDiv = '<div class="custom-dialog-review-count">' + numReviews + ' reviews</div>';
			dmJQuery(copyOfRollup).append(numReviewsDiv);
			dmJQuery(copyOfRollup).children('.pluck-review-create-review-rollup-wrap').remove();
			dmJQuery(copyOfRollup).children('.pluck-review-login-review-rollup-wrap').remove();
			dmJQuery(dialogWrapper).append(copyOfRollup);
			dmJQuery(dialogWrapper).append(rollupAttributes);	
		});
	},

	"customizeReviewsListWidget": function(){
		var reviews = dmJQuery('.pluck-review-full-review-single-review-wrap');
		// pluckCustomization.customizeReviewListRatings();
		// pluckCustomization.customizeReviewAttributeRatings();

		// fix the element order to allow for RWD
		var subheader = dmJQuery('.pluck-review-full-subheader');
		var filter = dmJQuery(subheader).find('.pluck-review-full-header-filter');
		var headline = dmJQuery(subheader).find('.pluck-review-full-subheader-headline');
		var sortLable = dmJQuery(subheader).find('.pluck-review-full-header-sort-label');
		var select = dmJQuery(subheader).find('pluck-review-full-header-sorting');

		dmJQuery(subheader).prepend(headline);
		dmJQuery(sortLable).insertAfter(headline);

		dmJQuery(subheader).attr('style', 'overflow:hidden;');

		// remove the last HR in the list of reviews
		reviews.last().css( 'border-bottom', '0px' );

		// loop through the reviews to make changes to each
		// dmJQuery.each( reviews, function( index, review ){
		// 	// move the helpful count above the title
		// 	var reviewDesc = dmJQuery(review).children('.pluck-review-full-review-desc');
		// 	var helpfulCount = dmJQuery(reviewDesc).children('.pluck-review-full-review-recommendations');
		// 	dmJQuery(reviewDesc).prepend(helpfulCount);

		// 	// move the recommend buttons above the action buttons
		// 	// var recommendButtons = dmJQuery(review)
		// 	// 	.children('.pluck-review-full-review-action-buttons')
		// 	// 	.children('.pluck-review-full-review-action-recommend');
		// 	// dmJQuery(review)
		// 	// 	.children('.pluck-review-full-review-action-buttons')
		// 	// 	.prepend(recommendButtons);

		// 	// change out the default helpful/not-helpful images
		// 	// var helpfulImage = dmJQuery(recommendButtons)
		// 	// 	.children('p')
		// 	// 	.children('.pluck-review-up-link').children('img');

		// 	// var notHelpfulImage = dmJQuery(recommendButtons)
		// 	// 	.children('p')
		// 	// 	.children('.pluck-review-down-link').children('img');
					
		// 	// if ( helpfulImage && helpfulImage[0] ) helpfulImage[0].src = 'images/thumbs-up.png';
		// 	// if ( notHelpfulImage && notHelpfulImage[0] ) notHelpfulImage[0].src = 'images/thumbs-down.png';


		// 	// fix the 'comments' link if there is only one comment
		// 	var hasComments = dmJQuery(review)
		// 		.children('.pluck-review-full-review-comment-wrap')
		// 		.children('p.pluck-review-full-review-comment-text')
		// 		.children('a.pluck-review-full-review-comment-show')
		// 		.children('.pluck-review-full-review-has-comments');

		// 	var numComments = dmJQuery(hasComments).children('.pluck-review-full-review-comment-count');

		// 	if( numComments && numComments[0] && numComments[0].innerHTML == '1' ){	
		// 		// console.dir( hasComments );
		// 		dmJQuery(hasComments).text( '1 Comment' );
		// 	}


		// });
	}, 

	"customizeReviewAttributeRatings": function(){
		var starGroups = dmJQuery('.pluck-review-attributesOutput');

		dmJQuery(starGroups).each(function( index, group ){
			var ratingDiv = dmJQuery(group).find('em');
			var leftVal = ratingDiv[0].style.left;
			var leftNumber = parseInt(leftVal.slice(0, leftVal.length -2));
			var numBars = leftNumber/15;
			console.log(numBars);
			dmJQuery(group).empty();

			// add the 'on' stars
			for( i=0; i<numBars; i++ ){
				dmJQuery(group).append('<i class="icon-stop stop-on" />');
			}

			// fill the rest with 'off stars'
			var remainingStars = pluckCustomization.numberOfStars - numBars;
			for ( i=0; i<remainingStars; i++ ){
				dmJQuery(group).append('<i class="icon-stop stop-off" />');
			}


		});
	},

	"customizeReviewListRatings": function(){
		var starGroups = dmJQuery('.pluck-review-starsOutput');

		dmJQuery(starGroups).each(function( index, group ){
			var ratingDiv = dmJQuery(group).find('.rating');
			var leftVal = ratingDiv[0].style.left;
			var leftNumber = parseInt(leftVal.slice(0, leftVal.length -2));
			var numStars = leftNumber/20;
			dmJQuery(group).empty();

			// add the 'on' stars
			for( i=0; i<numStars; i++ ){
				dmJQuery(group).append('<i class="icon-star-two star-on" />');
			}

			// fill the rest with 'off stars'
			var remainingStars = pluckCustomization.numberOfStars - numStars;
			for ( i=0; i<remainingStars; i++ ){
				dmJQuery(group).append('<i class="icon-star-two star-off" />');
			}


		});
	},

	"customizeLatestBlogPostDiscoWidget": function(){
		// add the > to the end of the title
		dmJQuery('.pluck-discovery-title').append('<span class="pluck-emerson-blog-title-append">&gt;</span>');

		// get the blog posts into an array
		var blogPosts = dmJQuery('.pluck-discovery-list').children();

		dmJQuery.each( blogPosts, function( index, post ){
				// get the link to the blog post
				var blogPostLink = post.children[0].children[0].href;
				var postAbstract = post.children[4];
				
				// add a 'see blog post' link to the end of the abstract
				dmJQuery(postAbstract).append('<span class="pluck-discovery-read-more"><a>See Blog Post</a></span>');
				postAbstract.children[0].children[0].href = blogPostLink;
		});
	}, 

	"customizeReviewComments": function( commentOnKey, commentOnKeyType ){
		var commDiv = dmJQuery('.pluck-comm[commentOnKey = "' + commentOnKey.commentOnKey + '"]');
		var reviewDiv = dmJQuery('.pluck-review-full-review-single-review-wrap[reviewKey = "' + commentOnKey.commentOnKey + '"]');
		var inputDiv = commDiv.children('.pluck-comm-comment-input');
		var loginDiv = commDiv.children('.pluck-login-comment-input');

		dmJQuery(commDiv).append(inputDiv);
		dmJQuery(commDiv).append(loginDiv);

		dmJQuery('.pluck-share-set.pluck-png').attr('title', 'Share');
		dmJQuery('.pluck-abuse-report.pluck-abuse-report-link.pluck-comm-report-abuse').attr('title', 'Report Abuse');
		
		dmJQuery.each( dmJQuery('.pluck-comm-abuse-area'), function( index, div ){
			var sibling = dmJQuery(div).prev();
			dmJQuery(sibling).append(div);
			var div = dmJQuery(sibling).find('a.pluck-abuse-report-link');
			dmJQuery(div).text('');
		} );
	},

	"customizeBlogPost": function(){
		pluckCustomization.resizeBlogWidget();
		// var paragraphs = dmJQuery(".pluck-publicBlogs-blogPost-description").children('p');
		// for ( i=0; i<paragraphs.length; i++ ){
		// 	if( dmJQuery(paragraphs[i]) 
		// 		&& dmJQuery(paragraphs[i]).children('a') 
		// 		&& dmJQuery(paragraphs[i]).children('a').length > 0 ){
		// 		var img = (dmJQuery(paragraphs[i]).children('a').children('img') );
		// 		dmJQuery(img[0]).attr('align', 'right');
		// 	} 
		// }
	},

	"customizeBlogRoll": function(){
		pluckCustomization.resizeBlogWidget();
		var blogAbstractWrapper = dmJQuery('.pluck-publicBlogs-blog-inner-wrap');

		dmJQuery.each( blogAbstractWrapper, function( index, abstract ){
			var parentDiv = abstract.parentElement;
			var blogPostKey = dmJQuery(parentDiv).attr('blogPostKey');
			dmJQuery(parentDiv).attr( 'id', blogPostKey );
			console.log(blogPostKey);
			var title = dmJQuery(abstract).find('.pluck-publicBlogs-blog-post-title');
			// console.log(title);

			dmJQuery('<div class="pluck-publicBlogs-blog-post-title-custom"></div>').insertBefore(parentDiv);
			dmJQuery(parentDiv).prev().append(title);
		} );
	},

	"customizeReviewsHelpfulWidget": function(){
		var favorableHeadline = dmJQuery('.pluck-review-most-helpful-favorable-headline');
		var criticalHeadline = dmJQuery('.pluck-review-most-helpful-critical-headline');
		var favorableContent = dmJQuery('.pluck-review-most-helpful-favorable-single-review-wrap');
		var criticalContent = dmJQuery('.pluck-review-most-helpful-critical-single-review-wrap');
		var newFavorableContentWrap = dmJQuery('<div class="pluck-review-most-helpful-full-content-column"></div>');
		var newCriticalContentWrap = dmJQuery('<div class="pluck-review-most-helpful-full-content-column"></div>');
		
		dmJQuery(newFavorableContentWrap).append(favorableHeadline);
		dmJQuery(newFavorableContentWrap).append(favorableContent);
		dmJQuery(newCriticalContentWrap).append(criticalHeadline);
		dmJQuery(newCriticalContentWrap).append(criticalContent);

		var mostHelpfulWidgetContainer = dmJQuery('.pluck-review-most-helpful').find('.pluck-review-full-content-wrap');

		dmJQuery(mostHelpfulWidgetContainer).prepend(newFavorableContentWrap);
		dmJQuery(mostHelpfulWidgetContainer).append(newCriticalContentWrap);
		pluckCustomization.resizeReviewsMostHelpfulWidget();
	}, 

	"customizeGroupsWidget": function(){
		// set up the hamburger menu for mobile
		pluckCustomization.setUpHamburgerMenu();
		// var headerInfo = dmJQuery('.')
		dmJQuery('.pluck-groups-header-groups').insertAfter('.pluck-groups-header-info');
	},

	"resizeBlogWidget": function(){
		var containerWidth = dmJQuery('.pluck-publicBlogs-wrapper').parentsUntil('.content-container', '.content').width();
		var proposedWidth = containerWidth * pluckCustomization.blogWidgetWidth;
		var w = (proposedWidth < pluckCustomization.siteMaxWidth) ? proposedWidth : pluckCustomization.siteMaxWidth;

		dmJQuery('.pluck-publicBlogs-wrapper').attr('style', 'width: ' + w + 'px');

		// console.dir(dmJQuery('.pluck-publicBlogs-blog-post-preview'));

		if(w < pluckCustomization.mobileBreak ){
			// var imgParentWidth = dmJQuery('.pluck-publicBlogs-blog-post-content').width();

			dmJQuery('.pluck-publicBlogs-blogPost-description img').attr('align', '');
			dmJQuery('.pluck-publicBlogs-blog-post-preview img').attr('align', '');
			// dmJQuery('.pluck-publicBlogs-blogPost-description img').attr('width', imgParentWidth);
			// dmJQuery('.pluck-publicBlogs-blog-post-preview img').attr('width', imgParentWidth);
		}else{
			dmJQuery('.pluck-publicBlogs-blogPost-description img').attr('float', pluckCustomization.blogImageDefaultFloat);
			dmJQuery('.pluck-publicBlogs-blog-post-preview img').attr('float', pluckCustomization.blogImageDefaultFloat);
			// dmJQuery('.pluck-publicBlogs-blogPost-description img').attr('width', '');
			// dmJQuery('.pluck-publicBlogs-blog-post-preview img').attr('width', '');
		}
	}, 

	"resizeReviewsMostHelpfulWidget": function(){
		// console.log('resizing RMH to ' + pluckCustomization.mostHelpfulWidgetWidth + ' of ' + dmJQuery(window).width() );
		
		var proposedWidth = dmJQuery(window).width() * pluckCustomization.blogWidgetWidth;
		var w = (proposedWidth < pluckCustomization.siteMaxWidth) ? proposedWidth : pluckCustomization.siteMaxWidth;
		var columns = dmJQuery('.pluck-review-most-helpful-full-content-column');

		dmJQuery('.pluck-review-main-area').attr('style', 'width: ' + w + 'px');
		if(w < pluckCustomization.mobileBreak ){
			columns.attr('style', 'float: none;');
			dmJQuery('.pluck-review-full-content-wrap').attr('style', 'height:' + columns.height()*2 + 'px;');
		}else{
			columns.attr('style', 'float: left;');
			dmJQuery('.pluck-review-most-helpful-button, pluck-review-most-helpful-favorable-button');
			dmJQuery('.pluck-review-full-content-wrap').attr('style', 'height:150px;');
		}
	}, 

	"resizeReviewsListWidget": function(){
		var parentWidth = dmJQuery('.content').width();

		dmJQuery('.pluck-review-full-content-wrap').attr('style', 'height:auto;');
		dmJQuery('.pluck-review-full-wrap').attr('style', 'width:' + parentWidth + 'px;overflow:hidden;');
		dmJQuery('.pluck-review-main-area').attr('style', 'width:' + parentWidth + 'px;overflow:hidden;');
	},

	"doResponsive": function(){
		pluckCustomization.resizeBlogWidget();
		pluckCustomization.resizeReviewsMostHelpfulWidget();
		pluckCustomization.resizeReviewsListWidget();
	},

	"setUpHamburgerMenu": function(){

		// insert the hamburger menu for mobile

		if( dmJQuery('.pluck-persona-first-navigation-holder').length ){
			var menuDiv = dmJQuery('.pluck-persona-first-navigation');
			var pluckDefaultMenuDiv = dmJQuery('.pluck-persona-first-navigation-holder');
			var menuItems = dmJQuery(pluckDefaultMenuDiv).find('li.pluck-persona-first-navigation-element');
		} else if( dmJQuery('.pluck-persona-third-navigation-holder').length ) {
			var menuDiv = dmJQuery('.pluck-persona-third-navigation');
			var pluckDefaultMenuDiv = dmJQuery('.pluck-persona-third-navigation-holder');
			var menuItems = dmJQuery(pluckDefaultMenuDiv).find('li.pluck-persona-third-navigation-element');
		} else if ( dmJQuery('.pluck-groups-navigation-holder').length ){
			var menuDiv = dmJQuery('.pluck-groups-navigation');
			var pluckDefaultMenuDiv = dmJQuery('.pluck-groups-navigation-holder');
			var menuItems = dmJQuery(pluckDefaultMenuDiv).find('li.pluck-groups-navigation-element');
		}

		var plckHamburgerHeader = dmJQuery('<hamburger-header> <div id="hamburger"> <div></div> <div></div> <div></div> </div> </hamburger-header>');
		var plckHamburgerMenu = dmJQuery('<nav class="hamburger-nav"></nav>');
		var plckHamburgerMenuUL = dmJQuery('<ul id="hamburger-nav-ul"></ul>');
		var additionalLinks = dmJQuery('.pluck-persona-first-global-navigation-link');
		
		// console.dir(menuDiv);

		dmJQuery(plckHamburgerMenu).insertBefore(menuDiv);
		dmJQuery(plckHamburgerMenu).append(plckHamburgerMenuUL);

		dmJQuery.each( menuItems, function(key, value){
			var clone = dmJQuery(value).clone();
			dmJQuery(plckHamburgerMenuUL).append(clone);
		});

		// add the settings and inbox links to the menu
		dmJQuery.each( additionalLinks , function( index, link ){
				settingsTab = '<li class="pluck-persona-first-navigation-element"><div class="pluck-persona-first-navigation-hover-off"><a href="' 
				+ link.href 
				+ '" class="pluck-persona-first-navigation-link" title="'
				+ link.innerText
				+ '">'
				+ link.innerText
				+ '</a></div></li>';

				dmJQuery( plckHamburgerMenuUL ).append( settingsTab );
		});

		dmJQuery(plckHamburgerHeader).insertBefore(plckHamburgerMenu);


    //click event for the hamburger menu
    dmJQuery("#hamburger").click(function () {

        dmJQuery('.plck-app-container-loaded').css('min-height', dmJQuery(window).height());

        var menuState = dmJQuery('.hamburger-nav').css('display');

        if (menuState == 'none'){
        	dmJQuery('.hamburger-nav').css('display', 'block');
        }else{
        	dmJQuery('.hamburger-nav').css('display', 'none');
        }


		});
	}, 

	"scrollLeft": function( menuMap ){
		try{
			var ulWidth = parseInt(dmJQuery('ul#scrolling-menubar').css('width').replace('px', ''));

			var maxLeftScroll = parseInt(menuMap[menuMap.liCount].menuOptionScrollPos) 
					- menuMap[menuMap.liCount].menuOptionWidth 
					- menuMap.menuOptionLeftMargin 
					+ ulWidth;
		
			if( menuMap[menuMap.nthChild].menuOptionScrollPos > maxLeftScroll ){
				var scrollTo = menuMap[menuMap.nthChild + pluckCustomization.pluckPersonaMenuScrollSpeed].menuOptionScrollPos;
				dmJQuery('ul#scrolling-menubar li').first().css('margin-left', scrollTo);
				menuMap.nthChild += pluckCustomization.pluckPersonaMenuScrollSpeed;
			}else{
				console.log(menuMap.menuOptionLeftMargin);
			}
		}catch (e){
			if(e.message.indexOf('undefined') < 0 )
				console.dir(e);	
		}
			
	}, 

	"scrollRight": function( menuMap ){
		var _nthChild = menuMap.nthChild - pluckCustomization.pluckPersonaMenuScrollSpeed;
		try{
			var scrollTo = menuMap[_nthChild].menuOptionScrollPos;
			dmJQuery('ul#scrolling-menubar li').first().css('margin-left', scrollTo);
			menuMap.nthChild = _nthChild;
		}catch( e ){
			if(e.message.indexOf('undefined') < 0 )
				console.dir(e);
		}
		
	},

	"setUpScrollingMenu": function(){

		if( dmJQuery('.pluck-persona-first-navigation-holder').length ){
			var menuDiv = dmJQuery('.pluck-persona-first-navigation');
			var pluckDefaultMenuDiv = dmJQuery('.pluck-persona-first-navigation-holder');
			var menuItems = dmJQuery(pluckDefaultMenuDiv).find('li.pluck-persona-first-navigation-element');
		} else if( dmJQuery('.pluck-persona-third-navigation-holder').length ) {
			var menuDiv = dmJQuery('.pluck-persona-third-navigation');
			var pluckDefaultMenuDiv = dmJQuery('.pluck-persona-third-navigation-holder');
			var menuItems = dmJQuery(pluckDefaultMenuDiv).find('li.pluck-persona-third-navigation-element');
		} else if ( dmJQuery('.pluck-groups-navigation-holder').length ){
			var menuDiv = dmJQuery('.pluck-groups-navigation');
			var pluckDefaultMenuDiv = dmJQuery('.pluck-groups-navigation-holder');
			var menuItems = dmJQuery(pluckDefaultMenuDiv).find('li.pluck-groups-navigation-element');
		}


		var plckScrollingMenuWrap = dmJQuery('<div class="scrolling-menu-wrap"></div>');
		var plckScrollingMenu = dmJQuery('<div class="scrolling-menu-container"></div>');
		var plckScrollingMenuUL = dmJQuery('<ul id="scrolling-menubar"></ul>');
		var scrollForward = dmJQuery('<span id="menuScrollforward"></span>');
		var scrollBack = dmJQuery('<span id="menuScrollback"></span>');
		var globalNavHolder = dmJQuery('.pluck-persona-first-global-navigation-holder');
		var personaSidebar = dmJQuery('.pluck-persona-sidebar-info-area');
		var additionalLinks = dmJQuery('.pluck-persona-first-global-navigation-link');
		
		// fix the width of the progress bar
		if( dmJQuery( '.pluck-persona-sidebar-profile-progressBar span em' ).length > 0 ){
			var progressLeft = dmJQuery('.pluck-persona-sidebar-profile-progressBar span em').css( 'left' ).replace('px', '');
			progressLeft = parseInt( progressLeft * 2 );
			dmJQuery('.pluck-persona-sidebar-profile-progressBar span em').css( 'left', progressLeft );
			console.log( progressLeft );
		}

		var map = { 'nthChild': 1, 'liCount': 0, 'menuOptionLeftMargin': menuOptionLeftMargin };
		

		dmJQuery(plckScrollingMenuWrap).insertBefore(menuDiv);
		dmJQuery(plckScrollingMenuWrap).append(plckScrollingMenu);
		dmJQuery(plckScrollingMenu).append(scrollBack);
		dmJQuery(plckScrollingMenu).append(plckScrollingMenuUL);
		dmJQuery(plckScrollingMenu).append(scrollForward);
		dmJQuery(globalNavHolder).insertBefore(personaSidebar);

		dmJQuery.each( menuItems, function(key, value){
			var tabLink = dmJQuery(value)[0].innerHTML;
			var customTab = "<li class='pluck-persona-first-navigation-element'>" + tabLink;
			dmJQuery(plckScrollingMenuUL).append(customTab);
		});


		// add the settings and inbox links to the menu
		dmJQuery.each( additionalLinks , function( index, link ){
				settingsTab = '<li class="pluck-persona-first-navigation-element"><div class="pluck-persona-first-navigation-hover-off"><a href="' 
				+ link.href 
				+ '" class="pluck-persona-first-navigation-link" title="'
				+ link.innerText
				+ '">'
				+ link.innerText
				+ '</a></div></li>';

				dmJQuery( plckScrollingMenuUL ).append( settingsTab );
		});


		dmJQuery(menuDiv).css( 'display', 'none' );

		var menuOptionLeftMargin = parseInt(dmJQuery('ul#scrolling-menubar li').css('margin-left').replace('px', ''));
		map.menuOptionLeftMargin = menuOptionLeftMargin;

		dmJQuery('ul#scrolling-menubar li').each( function( index ){
			index = parseInt(index) + 1;
			map.liCount += 1;
			var menuOptionWidth = dmJQuery('ul#scrolling-menubar li:nth-child(' + index +')')[0].clientWidth;
			var menuOptionText = dmJQuery('ul#scrolling-menubar li:nth-child(' + index +')')[0].innerText;
			var menuOptionScrollPos = ( index > 1 ) 
				? map[ index - 1 ].menuOptionScrollPos 
					- map[ index - 1 ].menuOptionWidth 
					- menuOptionLeftMargin
				: menuOptionLeftMargin;
			map[ index ] = {
				'menuOptionText': menuOptionText,
				'menuOptionWidth': menuOptionWidth, 
				'menuOptionScrollPos': menuOptionScrollPos
				};
		});


		dmJQuery( '#menuScrollforward' ).click( function(){
			pluckCustomization.scrollLeft( map ); 
		} );

		dmJQuery( '#menuScrollback' ).click( function(){
			pluckCustomization.scrollRight( map ); 
		} );

	}

} // pluckCustomization object

// pluckAppProxy.registerActivityCallback("PublicBlogsBlogRendered", pluckCustomization.customizeBlogRoll );
// pluckAppProxy.registerActivityCallback("PublicBlogsBlogPostRendered", pluckCustomization.customizeBlogPost );
// pluckAppProxy.registerActivityCallback("ReviewListRendered", pluckCustomization.customizeReviewsListWidget );
// pluckAppProxy.registerActivityCallback("ReviewRollupRendered", pluckCustomization.customizeReviewRollup );
pluckAppProxy.registerActivityCallback("CommentsRendered", pluckCustomization.customizeReviewComments );
// pluckAppProxy.registerActivityCallback("CommentCreate", pluckCustomization.refocusAfterCommentCreate );
// pluckAppProxy.registerActivityCallback("DiscoveryLoad", pluckCustomization.customizeLatestBlogPostDiscoWidget );
// pluckAppProxy.registerActivityCallback("PersonaLoad", pluckCustomization.customizePersonaWidget );
// pluckAppProxy.registerActivityCallback("ReviewMostHelpfulRendered", pluckCustomization.customizeReviewsHelpfulWidget );
// pluckAppProxy.registerActivityCallback("GroupsLoad", pluckCustomization.customizeGroupsWidget );
	
// pluckAppProxy.registerActivityCallback("ItemRatingsReact", function(){ console.log(confirm('are you sure?')); return false;} );
	
// dmJQuery(window).load(function(){
// 	dmJQuery(window).resize(pluckCustomization.doResponsive);
	
// });

// // intercept the call for the mini persona
// pluckAppProxy.origCallApp = pluckAppProxy.callApp;
// pluckAppProxy.callApp = function(path, params, cb) {
//   if(path){
//     // console.log(path);
//     if ( path == 'pluck/user/miniPersona.app' ){
//       return;
//     }else{
//     	pluckAppProxy.origCallApp(path, params, cb);
//     }
//   }
// } 

// pluckAppProxy.origCallApp = pluckAppProxy.callApp;
// pluckAppProxy.callApp = function(path, params, cb) {
//   if(path){
//     console.log(path);
//     if ( path == 'pluck/itemRatings.app' ){
//       console.log(confirm('are you sure?')); 
//       return false;
//     }else{
//     	pluckAppProxy.origCallApp(path, params, cb);
//     }
//   }
// }

// pluckAppProxy.registerActivityCallback("ItemRatingsReact", function(){ console.log(confirm('are you sure?')); return false;} );

