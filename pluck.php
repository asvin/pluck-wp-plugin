<?php
/**

Plugin Name: Pluck
Version: 1.0
Plugin URI: http://www.pluck.com/wordpress
Description: Pluck widgets.
Author: Pluck
Author URI: http://www.pluck.com
@package Pluck
Copyright 2016
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require_once( dirname( __FILE__ ) . '/vip/vip-helper.php' );

if ( ! class_exists( 'Pluck_Plugin' ) ) {

	/**
	 * Class Pluck_Plugin
	 *
	 * @author Sprinklr <mike.shelton@sprinklr.com>
	 */
	class Pluck_Plugin {

		/**
		 * Will have all our options, loaded upon init.
		 * @var String
		 */
		private $options;

		/**
		 * Plugin constructor
		 * @return void
		 */
		function __construct() {
			// Load options.
			$this->options = get_option( 'pluckplugin_settings' );

			// Enqueue scripts at the top.
			add_action( 'wp_enqueue_scripts', array( $this, 'pluckplugin_head_includes' ) );

			// Swap comments.
			add_filter( 'comments_template', array( $this, 'pluckplugin_comments_swap_actions' ) );

			// Single sign-on authentication.
			// add_action( 'wp_authenticate' , 'check_custom_authentication' );

			// Drop cookie on login.
			add_action( 'wp_login', array( $this, 'pluckplugin_drop_at_cookie' ), 10, 2 );

			// Load admin section.
			add_action( 'admin_menu', array( $this, 'pluckplugin_admin_actions' ) );

			// Init shortcode.
			add_action( 'init', array( $this, 'init' ) );

			// Settings link in plugin admin.
			add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ), 10, 4 );
			add_filter( 'network_admin_plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'plugin_action_links' ), 10, 4 );

			// Admin hooks.
			add_action( 'admin_enqueue_scripts', array( $this, 'add_admin_css' ) );
			add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
			add_action( 'save_post', array( $this, 'add_meta_box_save' ) );

			// Footer hooks.
			add_action( 'wp_footer', array( $this, 'add_inline_js' ) );

			// Custom URLs for pre and post callback URLS.
			add_filter( 'query_vars', array( $this, 'enhance_query_vars' ) );
			add_action( 'parse_request', array( $this, 'enhance_parse_request' ) );
		}

		/**
		 * Called upon activating the plugin.
		 *
		 * @return void
		 */
		public static function plugin_activation() {
			global $wp_rewrite;

			// Add custom pluck URLs automatically.
			add_rewrite_rule( 'pre-pluck-callback/?$', 'index.php?pre_pluck_callback=1', 'top' );
			add_rewrite_rule( 'post-pluck-callback/?$', 'index.php?post_pluck_callback=1', 'top' );
		    $wp_rewrite->flush_rules();
		}

		/**
		 * Function called when someone deactivates our plugin
		 * @return void
		 */
		public static function plugin_deactivation() {
			// Flush our custom rules.
			flush_rewrite_rules();
		}

		/**
		 * Registers the shortcode
		 * @return void
		 */
		function init() {

			// Register shortcode.
			add_shortcode( 'pluck-app', array( $this, 'pluck_widget_shortcode' ) );

			// Create a custom post type for Pluck logs.
		    register_post_type( 'pluck_log',
				array(
					'labels' => array(
						'name' => __( 'Pluck Logs' ),
						'singular_name' => __( 'Pluck Log' ),
					),
					'public' => true,
					'has_archive' => true,
					'show_in_menu' => false,
				)
			);
		}

		/**
		 * Include Pluck Libraries in Head
		 * @return void
		 */
		function pluckplugin_head_includes() {
			global $wpdb;

			$PluckSavedData = array();

			if ( $this->options ) {
				$PluckSavedData = $this->options;

				// Get default language.
				$lang = get_bloginfo( 'language' );

				// Base JS from Server.
				wp_enqueue_script( 'pluck-apps', $PluckSavedData['PluckDomain'] . '/ver1.0/Content/ua/scripts/pluckApps.js?skipCss=true&pluckLang='.$lang, array( 'jquery' ), '1.0.0' );

				// Custom JS for Plugin.
				wp_enqueue_script( 'pluck-widget', plugins_url( '/js/pluck-widget.js' , __FILE__ ) );

				// Pluck JS SDK.
				wp_enqueue_script( 'pluck-js-proxy', $PluckSavedData['PluckDomain'] . '/ver1.0/Direct/JavascriptSDKProxy', array( 'jquery' ), '1.0.0' );

				// Custom JS if set.
				if ( isset( $PluckSavedData['PluckCustomJS'] ) && ! empty( $PluckSavedData['PluckCustomJS'] ) ) {
					wp_enqueue_script( 'pluck-custom-js', $PluckSavedData['PluckCustomJS'] );
				}

				// Base styles from server.
				if ( isset( $PluckSavedData['PluckLegacyCSS'] ) && 'yes' === $PluckSavedData['PluckLegacyCSS'] ) {
					wp_enqueue_style( 'pluck-all', $PluckSavedData['PluckDomain'] . '/ver1.0/Content/ua/css/pluckAll.css', array(), '1.0.0' );
				}

				// Widget styles.
				wp_enqueue_style( 'pluck-widget-styles', plugins_url( '/css/pluck-styles.css' , __FILE__ ) );

				// Custom CSS if set.
				if ( isset( $PluckSavedData['PluckCustomCSS'] ) && ! empty( $PluckSavedData['PluckCustomCSS'] ) ) {
					wp_enqueue_style( 'pluck-custom-css', $PluckSavedData['PluckCustomCSS'] );
				}

				// Inject any inline styles.
				if ( isset( $PluckSavedData['PluckInlineCSS'] ) && '' !== $PluckSavedData['PluckInlineCSS'] ) {
					wp_add_inline_style( 'pluck-widget-styles', wp_strip_all_tags( $PluckSavedData['PluckInlineCSS'] ) );
				}
			}
		}

		/**
		 * Comments Swap Functionality
		 * @return String HTML string containing comment template
		 */
		function pluckplugin_comments_swap_actions() {
			global $post;
			global $wpdb;

			if ( 'open' === $post->comment_status ) {
				$PluckSavedData = array();
				if ( get_option( 'pluckplugin_settings' ) ) {
					$PluckSavedData = get_option( 'pluckplugin_settings' );
					if ( 'yes' === $PluckSavedData['PluckUseComments'] ) {
						return dirname( __FILE__ ) . '/comments.php';
					}
				}
			}
		}

		/**
		 * Drop authentication cookie
		 * @param  String  $user_login The user login.
		 * @param  WP_User $user       WordPress user object.
		 * @return void
		 */
		function pluckplugin_drop_at_cookie( $user_login, $user ) {
			global $wpdb;

			$PluckSavedData = array();
			if ( get_option( 'pluckplugin_settings' ) ) {
				$PluckSavedData = get_option( 'pluckplugin_settings' );

				if ( isset( $PluckSavedData['PluckDomain'] ) && isset( $PluckSavedData['PluckSharedSecret'] ) ) {
					$userId = $user->data->ID;
					$userName = $user->data->user_nicename;
					$userTime = '1';
					$userEmail = $user->data->user_email;
					$PwKey = $PluckSavedData['PluckSharedSecret'];

					// Get TLD from PluckDomain.
					$DomainParts = explode( '.', $PluckSavedData['PluckDomain'] );
					$CookieDomain = '.' . $DomainParts[1] . '.' . $DomainParts[2] . '';

					$Pieces = $userId. '.' . $userName . '.' . $userTime . '.' . $userEmail . '.' . $PwKey;
					$secret_hash = md5( $Pieces );
					$CookieString = 'u=' . $userId . '&a=' . $userName . '&e=' . $userEmail . '&t=' . $userTime . '&h=' . $secret_hash;

					// Check to see if using 'at' cookie name.
					$CookieName = 'at';
					if ( isset( $PluckSavedData['PluckCookieName'] ) ) {
						$CookieName = $PluckSavedData['PluckCookieName'];
					}

					$expire = time() + 60 * 60 * 24 * 30;
					setcookie( $CookieName, $CookieString, $expire, '/', $CookieDomain );
				}
			}
		}

		/**
		 * Admin screen title hook
		 * @return void
		 */
		function pluckplugin_admin_actions() {
			add_options_page( 'Pluck Plugin Settings', 'Pluck', 'manage_options', 'pluck-settings-page', array( $this, 'pluckplugin_admin' ) );
		}

		/**
		 * Admin settings screen
		 * @return void
		 */
		function pluckplugin_admin() {
			$activeTab = sanitize_text_field( wp_unslash( $_GET['tab'] ) );

			// Set default tab.
			if ( '' === $activeTab && ! in_array( $activeTab, array( 'core_options', 'display_options' ) ) ) {
				$activeTab = 'core_options';
			}
		?>
			<div class="wrap pluck-plugin-wrap">
				<h2><?php esc_html_e( 'Pluck Plugin Settings' ); ?></h2>
				<p class="description"><?php esc_html_e( 'Update the fields below with your Pluck information.' ); ?></p>
				<br/>

				<?php
				global $wpdb;

				if ( isset( $_POST['submit'] ) && isset( $_POST['_pluonce'] ) ) {
					if ( wp_verify_nonce( wp_unslash( $_POST['_pluonce'] ), 'save_pluck_settings' ) ) {
						$PluckSavedData = $_POST;

						// Some sanitization.
						$PluckSavedData['PluckDomain'] = esc_url_raw( $PluckSavedData['PluckDomain'] );
						$PluckSavedData['PluckSharedSecret'] = sanitize_text_field( $PluckSavedData['PluckSharedSecret'] );
						$PluckSavedData['PluckCookieName'] = sanitize_text_field( $PluckSavedData['PluckCookieName'] );
						$PluckSavedData['PluckWidgetTimeout'] = absint( $PluckSavedData['PluckWidgetTimeout'] );
						$PluckSavedData['PluckCustomCSS'] = esc_url_raw( $PluckSavedData['PluckCustomCSS'] );
						$PluckSavedData['PluckCustomJS'] = esc_url_raw( $PluckSavedData['PluckCustomJS'] );
						$PluckSavedData['PluckInlineCSS'] = wp_strip_all_tags( $PluckSavedData['PluckInlineCSS'] );
						$PluckSavedData['PluckInlineJS'] = wp_strip_all_tags( $PluckSavedData['PluckInlineJS'] );
						$PluckSavedData['PluckDebug'] = wp_strip_all_tags( $PluckSavedData['PluckDebug'] );

						// Store results in WP options table.
						update_option( 'pluckplugin_settings', $PluckSavedData );
					?>
						<div class="updated">
							<p><strong><?php esc_html_e( 'Settings saved.' ); ?></strong></p>
						</div>
					<?php
					} else {
					?>
						<div class="error">
							<p><strong><?php esc_html_e( 'An error occured. Please try again.' ); ?></strong></p>
						</div>
					<?php
					}
				}

				$PluckSavedData = get_option( 'pluckplugin_settings' );
				?>

				<h2 class="nav-tab-wrapper">
					<a href="<?php echo esc_attr( admin_url( 'options-general.php?page=pluck-settings-page' ) ); ?>&amp;tab=core_options" class="nav-tab pluck-nav-tab" data-rel="core_options" id="nav-core_options">Core Options</a>
	            	<a href="<?php echo esc_attr( admin_url( 'options-general.php?page=pluck-settings-page' ) ); ?>&amp;tab=display_options" class="nav-tab pluck-nav-tab" data-rel="display_options" id="nav-display_options">Display Options</a>
	        	</h2>
				<form action="" method="post">
					<table class="form-table pluck-settings-table widefat" id="table-core_options">
						<tbody>
							<tr>
								<td width="35%">
									<label><?php esc_html_e( 'Pluck Enabled' ); ?></label>
								</td>
								<td width="65%">
									<?php
									if ( 'yes' === $PluckSavedData['PluckEnabled'] || '' === $PluckSavedData['PluckEnabled'] ) :
										$checkedPluckEnabledYes = 'checked="checked"';
										$checkedPluckEnabledNo = '';
									else :
										$checkedPluckEnabledNo = 'checked="checked"';
										$checkedPluckEnabledYes = '';
									endif;
									?>
									<input type="radio" name="PluckEnabled" id="pluck-enabled-yes" value="yes" <?php echo esc_attr( $checkedPluckEnabledYes ); ?> /> 
									<label for="pluck-enabled-yes" class="label-value"><?php esc_html_e( 'Yes' ); ?></label>
									<br />
									<input type="radio" name="PluckEnabled" id="pluck-enabled-no" value="no" <?php echo esc_attr( $checkedPluckEnabledNo ); ?> /> 
									<label for="pluck-enabled-no" class="label-value"><?php esc_html_e( 'No' ); ?></label>
								</td>
							</tr>

							<tr>
								<td>
									<label><?php esc_html_e( 'Pluck Domain' ); ?></label>
								</td>
								<td>
									<input type="text" name="PluckDomain" value="<?php echo esc_attr( $PluckSavedData['PluckDomain'] ); ?>" size="40" required />
									<br/>
									<small><?php esc_html_e( 'Example: https://pluck.mysite.com' ); ?></small>
								</td>
							</tr>

							<tr>
								<td>
									<label><?php esc_html_e( 'Pluck Shared Secret' ); ?></label>
								</td>
								<td>
									<input type="text" name="PluckSharedSecret" value="<?php echo esc_attr( $PluckSavedData['PluckSharedSecret'] ); ?>" size="40" required />
								</td>
							</tr>

							<tr>
								<td>
									<label><?php esc_html_e( 'Pluck Cookie Name' ); ?></label>
								</td>
								<td>
									<?php
									if ( isset( $PluckSavedData['PluckCookieName'] ) ) :
										$pluckCookieName = $PluckSavedData['PluckCookieName'];
									else :
										$pluckCookieName = 'at';
									endif;
									?>
									<input type="text" name="PluckCookieName" value="<?php echo esc_attr( $pluckCookieName ); ?>" size="10" />
									<br/>
									<small><?php esc_html_e( 'Note: This is rarely changed.' ); ?></small>
								</td>
							</tr>

							<tr>
								<td>
									<label><?php esc_html_e( 'Server Side Widget Timeout' ); ?></label>
								</td>
								<td>
									<?php
									if ( isset( $PluckSavedData['PluckWidgetTimeout'] ) ) :
										$pluckTimeout = $PluckSavedData['PluckWidgetTimeout'];
									else :
										$pluckTimeout = '4000';
									endif;
									?>
									<input type="text" name="PluckWidgetTimeout" value="<?php echo esc_attr( $pluckTimeout );  ?>" size="10" />
									<br/>
									<small><?php esc_html_e( 'in milliseconds. 4000 milliseconds = 4 seconds.' ); ?></small>
								</td>
							</tr>

							<tr>
								<td valign="top">
									<label><?php esc_html_e( 'Use Pluck Comments' ); ?></label>
								</td>
								<td>
									<?php
									if ( 'yes' === $PluckSavedData['PluckUseComments'] || '' === $PluckSavedData['PluckUseComments'] ) :
										$checkedPluckCommentsYes = 'checked="checked"';
										$checkedPluckCommentsNo = '';
									else :
										$checkedPluckCommentsNo = 'checked="checked"';
										$checkedPluckCommentsYes = '';
									endif;
									?>
									<input type="radio" name="PluckUseComments" id="pluck-comments-yes" value="yes" <?php echo esc_attr( $checkedPluckCommentsYes ); ?> /> 
									<label for="pluck-comments-yes" class="label-value"><?php esc_html_e( 'Yes' ); ?></label>
									<br />
									<input type="radio" name="PluckUseComments" id="pluck-comments-no" value="no" <?php echo esc_attr( $checkedPluckCommentsNo ); ?> /> 
									<label for="pluck-comments-no" class="label-value"><?php esc_html_e( 'No' ); ?></label>
								</td>
							</tr>

							<tr>
								<td valign="top">
									<label><?php esc_html_e( 'Debugging mode' ); ?></label>
								</td>
								<td>
									<?php
									if ( 'yes' === $PluckSavedData['PluckDebug'] || '' === $PluckSavedData['PluckDebug'] ) :
										$checkedPluckDebugYes = 'checked="checked"';
										$checkedPluckDebugNo = '';
									else :
										$checkedPluckDebugNo = 'checked="checked"';
										$checkedPluckDebugYes = '';
									endif;
									?>
									<input type="radio" name="PluckDebug" id="pluck-debug-yes" value="yes" <?php echo esc_attr( $checkedPluckDebugYes ); ?> /> 
									<label for="pluck-debug-yes" class="label-value"><?php esc_html_e( 'Yes' ); ?></label>
									<br />
									<input type="radio" name="PluckDebug" id="pluck-debug-no" value="no" <?php echo esc_attr( $checkedPluckDebugNo ); ?> /> 
									<label for="pluck-debug-no" class="label-value"><?php esc_html_e( 'No' ); ?></label>
								</td>
							</tr>
						</tbody>
					</table>

					<table class="form-table pluck-settings-table widefat" id="table-display_options">
						<tbody>
							<tr>
								<td valign="top" width="35%">
									<label><?php esc_html_e( 'Load Legacy Pluck CSS' ); ?></label>
								</td>
								<td>
									<?php
									if ( 'yes' === $PluckSavedData['PluckLegacyCSS'] || '' === $PluckSavedData['PluckLegacyCSS'] ) :
										$checkedPluckLegacyCssYes = 'checked="checked"';
										$checkedPluckLegacyCssNo = '';
									else :
										$checkedPluckLegacyCssNo = 'checked="checked"';
										$checkedPluckLegacyCssYes = '';
									endif;
									?>
									<input type="radio" name="PluckLegacyCSS" id="pluck-legacy-css-yes" value="yes" <?php echo esc_attr( $checkedPluckLegacyCssYes ); ?> /> 
									<label for="pluck-legacy-css-yes" class="label-value"><?php esc_html_e( 'Yes' ); ?></label>
									<br />
									<input type="radio" name="PluckLegacyCSS" id="pluck-legacy-css-no" value="no" <?php echo esc_attr( $checkedPluckLegacyCssNo ); ?> /> 
									<label for="pluck-legacy-css-no" class="label-value"><?php esc_html_e( 'No' ); ?></label>
								</td>
							</tr>

							<tr>
								<td>
									<label><?php esc_html_e( 'Custom CSS File' ); ?></label>
								</td>
								<td>
									<?php
									$pluckCustomCss = '';
									if ( isset( $PluckSavedData['PluckCustomCSS'] ) ) {
										$pluckCustomCss = $PluckSavedData['PluckCustomCSS'];
									}
									?>
									<input type="text" name="PluckCustomCSS" value="<?php echo esc_attr( $pluckCustomCss ); ?>" size="40" placeholder="http://" />
									<br/>
									<small><?php esc_html_e( 'Optional. Full URL path required.' ); ?></small>
								</td>
							</tr>

							<tr>
								<td>
									<label><?php esc_html_e( 'Custom JavaScript File' ); ?></label>
								</td>
								<td>
									<?php
									$pluckCustomJs = '';
									if ( isset( $PluckSavedData['PluckCustomJS'] ) ) {
										$pluckCustomJs = $PluckSavedData['PluckCustomJS'];
									}
									?>
									<input type="text" name="PluckCustomJS" value="<?php echo esc_attr( $pluckCustomJs ); ?>" size="40" placeholder="http://" />
									<br/>
									<small><?php esc_html_e( 'Optional. Full URL path required.' ); ?></small>
								</td>
							</tr>

							<tr>
								<td>
									<label><?php esc_html_e( 'Custom Inline CSS' ); ?></label>
								</td>
								<td>
									<div id="pluck-inline-css-editor"></div>
									<textarea name="PluckInlineCSS" id="pluck-inline-css-textarea"><?php echo esc_textarea( $PluckSavedData['PluckInlineCSS'] ); ?></textarea>
									<small><?php esc_html_e( 'Without opening and closing style tags' ); ?></small>
								</td>
							</tr>

							<tr>
								<td>
									<label><?php esc_html_e( 'Custom Inline JS' ); ?></label>
								</td>
								<td>
									<div id="pluck-inline-js-editor"></div>
									<textarea name="PluckInlineJS" id="pluck-inline-js-textarea"><?php echo esc_textarea( wp_unslash( $PluckSavedData['PluckInlineJS'] ) ); ?></textarea>
									<small><?php esc_html_e( 'Without opening and closing script tags' ); ?></small>
								</td>
							</tr>
						</tbody>
					</table>

					<table class="form-table widefat pluck-actions-table">
						<tfoot>
							<tr>
								<td width="35%"></td>
								<td>
									<?php wp_nonce_field( 'save_pluck_settings', '_pluonce' );?>
									<?php submit_button(); ?>
								</td>
							</tr>
						</tfoot>
					</table>
				</form>
			</div>
			<script type="text/javascript">
				jQuery('#nav-<?php echo esc_html( $activeTab ); ?>').addClass('nav-tab-active');
				jQuery('#table-<?php echo esc_html( $activeTab ); ?>').show();

				jQuery(document).on('click', '.pluck-nav-tab', function(e){
					e.preventDefault();

					// get desired tab we want to show				
					var rel = jQuery(this).data('rel');

					// hide tables and reset menu
					jQuery('.pluck-nav-tab').removeClass('nav-tab-active');
					jQuery('.pluck-settings-table').hide();

					// show desired table and set menu active
					jQuery('#nav-' + rel).addClass('nav-tab-active');
					jQuery('#table-' + rel).show();
				});

				var editor = ace.edit('pluck-inline-css-editor');
				editor.setTheme('ace/theme/monokai');
				editor.getSession().setMode('ace/mode/css');

				editor.getSession().setValue(jQuery('#pluck-inline-css-textarea').val());
				editor.getSession().on('change', function(){
	  				jQuery('#pluck-inline-css-textarea').val(editor.getSession().getValue());
				});

				var jsEditor = ace.edit('pluck-inline-js-editor');
				jsEditor.setTheme('ace/theme/monokai');
				jsEditor.getSession().setMode('ace/mode/javascript');

				jsEditor.getSession().setValue(jQuery('#pluck-inline-js-textarea').val());
				jsEditor.getSession().on('change', function(){
	  				jQuery('#pluck-inline-js-textarea').val(jsEditor.getSession().getValue());
				});
			</script>
		<?php
		}

		/**
		 * Set current page URL
		 * @return  String the url encoded url
		 */
		function set_current_page_url() {
			$pageURL = 'http';

			/*
			If ($_SERVER["HTTPS"] == "on") {
				$pageURL .= "s";
			}
			*/

			$pageURL .= '://';

			if ( '80' !== $_SERVER['SERVER_PORT'] ) {
				$pageURL .= $_SERVER['SERVER_NAME']. ':' . $_SERVER['SERVER_PORT'] . $_SERVER['REQUEST_URI'];
			} else {
				$pageURL .= $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
			}

			return urlencode( $pageURL );
		}

		/**
		 * Widget function
		 * @param  Array $attributes Array containing attributes.
		 * @return String Server response.
		 */
		function widget( $attributes ) {
			global $post;

			// Get saved data from DB.
			$PluckSavedData = array();
			if ( $this->options ) {
				$PluckSavedData = $this->options;
			}

			// Is Pluck enabled?
			if ( 'yes' !== $PluckSavedData['PluckEnabled'] ) {
				return;
			}

			// Pull application.
			if ( ! empty( $attributes['app'] ) ) {
				$application = $attributes['app'];
				unset( $attributes['app'] );
			} else {
				return 'Pluck Error: \'app\' property is required.';
			}

			$action_url = $PluckSavedData['PluckDomain'] . '/ver1.0/';

			// Handle Allegro Forums + load appropriate library.
			if ( 'forums' === strtolower( $application ) ) {
				$action_url .= 'pluckui/forums?';
				wp_enqueue_script( 'pluck-js-allegro', $PluckSavedData['PluckDomain'] . '/ver1.0/content/pluckui/scripts/PluckUI.min.js' );
			} else {
				$action_url .= strtolower( $application ) . '.app?' ;
			}

			// Autopopulate post ID if not specified (comments and reviews).
			if ( ( 'pluck/comments' === strtolower( $application ) ) && ! array_key_exists( 'plckCommentOnKey', $attributes ) ) {
				$attributes['plckCommentOnKey'] = $post->ID;
			} elseif ( ( 'pluck/reviews/list' === strtolower( $application ) ) && ! array_key_exists( 'plckReviewOnKey', $attributes ) ) {
				$attributes['plckReviewOnKey'] = $post->ID;
			} elseif ( ( 'pluck/reviews/rollup' === strtolower( $application ) ) && ! array_key_exists( 'plckReviewOnKey', $attributes ) ) {
				$attributes['plckReviewOnKey'] = $post->ID;
			} elseif ( ( 'pluck/reviews/mostHelpful' === strtolower( $application ) ) && ! array_key_exists( 'plckReviewOnKey' , $attributes ) ) {
				$attributes['plckReviewOnKey'] = $post->ID;
			}

			// Add all of the arguments.
			if ( $attributes ) {
				foreach ( $attributes as $key => $value ) {
					$action_url .= $key . '=' . $value . '&';
				}
			}

			// Add the current page url.
			$action_url .= 'clientUrl=' . $this->set_current_page_url();

			// Get timeout in seconds.
			$timeoutSeconds = (absint( $PluckSavedData['PluckWidgetTimeout'] ) / 1000);

			// Cookies creation.
			$cookies = array();
			foreach ( $_COOKIE as $name => $value ) {
				$cookies[] = new WP_Http_Cookie( array( 'name' => $name, 'value' => $value ) );
			}

			// Make request.
			$response = vip_safe_wp_remote_get( $action_url, false, 1, $timeoutSeconds, 20, array( 'cookies' => $cookies ) );

			if ( is_wp_error( $response ) ) {
				return 'Error displaying Pluck widget.';
			} else {
				return wp_remote_retrieve_body( $response );
			}
		}

		/**
		 * Pluck widget shortcode
		 * @param  Array $attributes Array containing attributes.
		 * @return HTML              Pluck widget.
		 */
		function pluck_widget_shortcode( $attributes ) {

			// If the key is not set
			if ( ! isset( $attributes['plckreviewonkey'] ) ) {
				global $post;

				// Try to get custom pluck id from custom data.
				$metaPluckId = get_post_meta( $post->ID, 'meta_pluck_id', true );

				if ( $metaPluckId ) {
					$attributes['plckreviewonkey'] = get_post_meta( $post->ID, 'meta_pluck_id', true );
				} else {
					$attributes['plckreviewonkey'] = $post->ID;
				}
			}

			// Get attributes.
			$shortcode_atts = shortcode_atts( array(
				'app' => 'pluck/comments',
				'params' => '',
			), $attributes );

			return $this->widget( $attributes );
		}

		/**
		 * Adds settings menu in plugin admin
		 * @param  String $links The link.
		 * @return Array
		 */
		function plugin_action_links( $links ) {
			array_unshift( $links, '<a href="' . admin_url( 'options-general.php?page=pluck-settings-page' ) . '">Settings</a>' );
			array_unshift( $links, '<a href="' . admin_url( 'edit.php?post_type=pluck_log' ) . '">Logs</a>' );

			return $links;
		}

		/**
		 * Adds admin CSS
		 * @return void
		 */
		function add_admin_css() {
			wp_enqueue_script( 'pluck-ace-editor', 'https://cdnjs.cloudflare.com/ajax/libs/ace/1.2.3/ace.js', array(), '1.2.3' );

			wp_register_style( 'pluck-admin-css', plugins_url( 'css/admin-pluck-styles.css', __FILE__ ) );
			wp_enqueue_style( 'pluck-admin-css' );
		}

		/**
		 * Adds inline JS
		 * @return void
		 */
		function add_inline_js() {
			// Inject any inline JS.
			if ( isset( $this->options['PluckInlineJS'] ) && '' !== $this->options['PluckInlineJS'] ) {
				echo '<script type="text/javascript">'. esc_html( wp_unslash( $this->options['PluckInlineJS'] ) ) . '</script>';
			}
		}

		/**
		 * Adds a Pluck metabox in posts, pages. Useful to use a custom pluck id
		 * @return void
		 */
		function add_meta_box() {
			add_meta_box( 'pluck-meta-box-wrapper', 'Pluck', array( $this, 'add_meta_box_callback' ), 'post', 'side', 'default' );
		}

		/**
		 * Callback for the meta box
		 * @param Post $post [description]
		 */
		function add_meta_box_callback( $post ) {
			$values = get_post_custom( $post->ID );
			$metaPluckId = '';
			if ( isset( $values['meta_pluck_id'] ) ) {
				$metaPluckId = esc_attr( sanitize_text_field( $values['meta_pluck_id'][0] ) );
			}

			// We'll use this nonce field later on when saving.
			wp_nonce_field( 'meta_pluck_id_nonce', '_plidonce' );
			?>
			<label for="meta_pluck_id">Custom Pluck ID</label>
    		<input type="text" name="meta_pluck_id" id="meta_pluck_id" value="<?php echo $metaPluckId; ?>" />
			<?php
		}

		/**
		 * Callback upon saving a post
		 * @param  Integer $post_id The post ID
		 * @return void
		 */
		function add_meta_box_save( $post_id ) {
			// Bail if we're doing an auto save.
		    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		    	return;
		    }

		    // If our nonce isn't there, or we can't verify it, bail.
		    if ( ! isset( $_POST['_plidonce'] ) || ! wp_verify_nonce( $_POST['_plidonce'], 'meta_pluck_id_nonce' ) ) {
		    	return;
		    }

		    // If our current user can't edit this post, bail.
		    if ( ! current_user_can( 'edit_post' ) ) {
		    	return;
		    }

		    // Make sure your data is set before trying to save it.
			if ( isset( $_POST['meta_pluck_id'] ) ) {
				update_post_meta( $post_id, 'meta_pluck_id', wp_strip_all_tags( $_POST['meta_pluck_id'] ) );
			}
		}

		/**
		 * Allows query vars to be added.
		 * @param  Array $query_vars WordPress URL variables.
		 * @return Array
		 */
		public static function enhance_query_vars( $query_vars ) {
		    $query_vars[] = 'pre_pluck_callback';
		    $query_vars[] = 'post_pluck_callback';
		    return $query_vars;
		}

		/**
		 * This action hook is executed at the end of WordPress's built-in request parsing method
		 * @param Object $wp WordPress Object.
		 * @return void
		 */
		public static function enhance_parse_request( &$wp ) {
			// Check if we are requesting which callback url.
	    	if ( array_key_exists( 'pre_pluck_callback', $wp->query_vars ) ) {
	    		// POST data contains the following keys:
	    		// fbUser, pluckUsers, access_token, sig.
				// (
				// [fbUser] => {"name":"Asvin Enm Balloo","id":"10156633578935321"}
				// [pluckUsers] => [{"Age":"","Sex":-1,"AboutMe":"","Location":"","ExtendedProfile":[],"CustomAnswers":[],"NumberOfMessages":0,"NumberOfFriends":0,"NumberOfPendingFriends":0,"NumberOfComments":0,"NumberOfForumPosts":0,"MessagesOpenToEveryone":false,"PersonaPrivacyMode":0,"DateOfBirth":"0001-01-01T00:00:00","CommentsTabVisible":true,"PhotosTabVisible":true,"IsEmailNotificationsEnabled":false,"DisableAllEmailNotifications":false,"SelectedStyleId":"","Signature":null,"AwardStatus":{"Badges":[],"LeaderboardRankings":[],"Activities":[],"ObjectType":"Models.PointsAndBadging.AwardStatus","CacheDependencyKeys":null},"AbuseCounts":{"AbuseReportCount":0,"CurrentUserHasReportedAbuse":false,"ContentIsStandardAbuse":true,"ContentExceedsAbuseThreshold":false,"TargetKey":{"Key":"4","ObjectType":"Users.Keys.UserProfileKey"},"ObjectType":"Models.Moderation.AbuseCount","CacheDependencyKeys":null},"RecommendationCounts":{"NumberOfRecommendations":0,"CurrentUserHasRecommended":false,"TargetKey":{"Key":"4","ObjectType":"Users.Keys.UserProfileKey"},"ObjectType":"Models.Reactions.RecommendationCount","CacheDependencyKeys":null},"ImageId":"00000000-0000-0000-0000-000000000000","IsAnonymous":false,"ProductVersion":null,"PeqContentKey":"users/4","AvatarPhotoUrl":"http://pluckstage.wyndhamtimeshare.com/ver1.0/../static/images/no-user-image.gif","AvatarPhotoID":"00000000-0000-0000-0000-000000000000","UserKey":{"Key":"4","ObjectType":"Models.Users.UserKey"},"MixedUserKey":"4","DisplayName":"asvinballoo","Email":"","UserTier":3,"AdministrativeTier":0,"IsSiteAdmin":false,"PersonaUrl":"http://stage.wyndhamtimeshare.com/Pages/PAS/Persona?UID=4&plckUserId=4","IsBlocked":false,"IsUnderReview":false,"ExternalUserIds":[{"Key":"Facebook","Value":"10156633578935321","ObjectType":"Models.Common.SiteLifeKeyValuePair"}],"CreatedOn":"2016-02-10T05:30:58.9167238","LastUpdated":"2016-02-12T18:52:57.9807554","FriendshipStatus":{"IsEnemy":false,"IsFriend":false,"IsPendingFriend":false,"ExecutingUserIsEnemy":false,"ExecutingUserIsFriend":false,"ExecutingUserIsPendingFriend":false,"TargetKey":{"Key":"4","ObjectType":"Models.Users.UserKey"},"ObjectType":"Models.Users.FriendshipStatus","CacheDependencyKeys":null},"UserStatus":"","IsOffsiteUser":false,"Imported":3,"ImporterType":null,"SourceSite":null,"ObjectType":"Models.Users.User","CacheDependencyKeys":[{"Key":"4","ObjectType":"Models.Users.UserKey"},{"Key":"4","ObjectType":"Users.Keys.UserProfileKey"}]}]
				// [access_token] => CAAJLP4LCovgBANZCvxVYYS4ANTg6O4zwNbZCZC1cFkv8TfMi6PZAn1VZA2E3rmej4ZAkzpgGh6m02O9pjlWN4J4Dzfe0pebdNcYlsKfBV3zPNBjCdjmQiMu7Ul1oyQvl5LqKqeGOSF4IhywzHN8azccAEGcLct8ZA8VmafkwiGZCV2Iv0ZAvCfgh416sMI0DEerhhKHVpznitun8xSr7I2b0W
				// [sig] => 1e20ec05b20d27906221eb704c20ee4f
				// )
				// curl:
				// curl --data 'fbUser={"name":"Asvin Enm Balloo","id":"10156633578935321"}&pluckUsers=[{"Age":"","Sex":-1,"AboutMe":"","Location":"","ExtendedProfile":[],"CustomAnswers":[],"NumberOfMessages":0,"NumberOfFriends":0,"NumberOfPendingFriends":0,"NumberOfComments":0,"NumberOfForumPosts":0,"MessagesOpenToEveryone":false,"PersonaPrivacyMode":0,"DateOfBirth":"0001-01-01T00:00:00","CommentsTabVisible":true,"PhotosTabVisible":true,"IsEmailNotificationsEnabled":false,"DisableAllEmailNotifications":false,"SelectedStyleId":"","Signature":null,"AwardStatus":{"Badges":[],"LeaderboardRankings":[],"Activities":[],"ObjectType":"Models.PointsAndBadging.AwardStatus","CacheDependencyKeys":null},"AbuseCounts":{"AbuseReportCount":0,"CurrentUserHasReportedAbuse":false,"ContentIsStandardAbuse":true,"ContentExceedsAbuseThreshold":false,"TargetKey":{"Key":"4","ObjectType":"Users.Keys.UserProfileKey"},"ObjectType":"Models.Moderation.AbuseCount","CacheDependencyKeys":null},"RecommendationCounts":{"NumberOfRecommendations":0,"CurrentUserHasRecommended":false,"TargetKey":{"Key":"4","ObjectType":"Users.Keys.UserProfileKey"},"ObjectType":"Models.Reactions.RecommendationCount","CacheDependencyKeys":null},"ImageId":"00000000-0000-0000-0000-000000000000","IsAnonymous":false,"ProductVersion":null,"PeqContentKey":"users/4","AvatarPhotoUrl":"http://pluckstage.wyndhamtimeshare.com/ver1.0/../static/images/no-user-image.gif","AvatarPhotoID":"00000000-0000-0000-0000-000000000000","UserKey":{"Key":"4","ObjectType":"Models.Users.UserKey"},"MixedUserKey":"4","DisplayName":"asvinballoo","Email":"","UserTier":3,"AdministrativeTier":0,"IsSiteAdmin":false,"PersonaUrl":"http://stage.wyndhamtimeshare.com/Pages/PAS/Persona?UID=4&plckUserId=4","IsBlocked":false,"IsUnderReview":false,"ExternalUserIds":[{"Key":"Facebook","Value":"10156633578935321","ObjectType":"Models.Common.SiteLifeKeyValuePair"}],"CreatedOn":"2016-02-10T05:30:58.9167238","LastUpdated":"2016-02-12T18:52:57.9807554","FriendshipStatus":{"IsEnemy":false,"IsFriend":false,"IsPendingFriend":false,"ExecutingUserIsEnemy":false,"ExecutingUserIsFriend":false,"ExecutingUserIsPendingFriend":false,"TargetKey":{"Key":"4","ObjectType":"Models.Users.UserKey"},"ObjectType":"Models.Users.FriendshipStatus","CacheDependencyKeys":null},"UserStatus":"","IsOffsiteUser":false,"Imported":3,"ImporterType":null,"SourceSite":null,"ObjectType":"Models.Users.User","CacheDependencyKeys":[{"Key":"4","ObjectType":"Models.Users.UserKey"},{"Key":"4","ObjectType":"Users.Keys.UserProfileKey"}]}]&access_token=CAAJLP4LCovgBANZCvxVYYS4ANTg6O4zwNbZCZC1cFkv8TfMi6PZAn1VZA2E3rmej4ZAkzpgGh6m02O9pjlWN4J4Dzfe0pebdNcYlsKfBV3zPNBjCdjmQiMu7Ul1oyQvl5LqKqeGOSF4IhywzHN8azccAEGcLct8ZA8VmafkwiGZCV2Iv0ZAvCfgh416sMI0DEerhhKHVpznitun8xSr7I2b0W&sig=1e20ec05b20d27906221eb704c20ee4f' http://localhost.pluck.com/pre-pluck-callback/
				// Get POST variables.
				$fbUser = json_decode( wp_unslash( $_POST['fbUser'] ), true );
				$pluckUsers = json_decode( wp_unslash( $_POST['pluckUsers'] ), true );
				$accessToken = sanitize_text_field( $_POST['access_token'] );
				$sig = sanitize_text_field( $_POST['sig'] );

				// Check if we have the data.
				if ( $fbUser && $pluckUsers && $accessToken && $sig ) {
					// We generate an email for this user since FB is not returning the email. Should check permission.
					$email = $fbUser['id'] . '@' . $_SERVER['SERVER_NAME'];
					$name = $fbUser['name'];
					$avatarUrl = $pluckUsers[0]['AvatarPhotoUrl'];

					// Check existing user id.
					$userId = username_exists( $email );

					// Check if user exists.
					if ( null == $userId ) {
						// Generate the password and create the user.
						$password = wp_generate_password( 12, false );

						$userData = array(
							'user_pass' => $password,
							'user_login' => $email,
							'user_nicename' => sanitize_title( $name ),
							'user_email' => $email,
							'first_name' => $name,
							'last_name' => '',
							'role' => 'subscriber',
						);
						$userId = wp_insert_user( $userData );

						// Add some meta data for that user.
						update_user_attribute( $userId, 'created_by', 'Pluck Social SignOn' );

						self::log( 'New User ' . $name . ' Created', 'User not found, Pluck automatically creating one with the following properties' . "\n\r". wp_json_encode( $userData ) );
					}

					// On success.
					if ( ! is_wp_error( $userId ) ) {
						// Generate output.
						$request = array(
							'SignedUserToken' => array(
								'UserKey' => $userId,
								'DisplayName' => $name,
								'Email' => $email,
								'AvatarUrl' => $avatarUrl,
								'Sig' => md5( $userId . $name . $email . $avatarUrl ),
							),
						);

						// Call Pluck servers now
						$settings = get_option( 'pluckplugin_settings' );

						// Make query.
						$response = wp_remote_post( $settings['PluckDomain'], array(
							'method' => 'POST',
							array(
								'body' => array(
									$request
								),
							),
						) );

						if ( 200 === $response['response']['code'] ) {
							// Successful query.
						}
					}
				}

				// log requests and response
				self::log(
					'Pre User Callback For ' . $name,
					'POST data: ' . "\r\n" .
					wp_json_encode( wp_unslash( $_POST ) ) . "\r\n\r\n" .
					'Response from ' . $settings['PluckDomain'] . ':' . "\r\n" .
					wp_json_encode( $response['response'] ) . "\r\n\r\n" .
					'Request made: ' . "\r\n" .
					wp_json_encode( $request )
				);

	    		exit;
	    	} else if ( array_key_exists( 'post_pluck_callback', $wp->query_vars ) ) {
	    		// @TODO: drop cookie or send data back to pluck or include a php file: include dirname( __FILE__ ) . '/your-file.php';
	    		exit;
	    	}

	    	return;
		}

		/**
		 * Simple logging mechanism to log anything.
		 * @param  String $title   The title of the post.
		 * @param  String $content The Description.
		 * @return boolean         Successful or not.
		 */
		public static function log( $title, $content ) {
			$settings = get_option( 'pluckplugin_settings' );

			// Check if we are in debug mode.
			if ( 'yes' === $settings['PluckDebug'] ) {
				return wp_insert_post(
					array(
						'post_title' => $title,
						'post_content' => $content,
						'post_type' => 'pluck_log',
						'post_status' => 'private',
					)
				);
			} else {
				return false;
			}
		}
	}
}

// Instantiate plugin.
new Pluck_Plugin();

// Registers all plugin activation/deactivation hooks.
register_activation_hook( __FILE__, array( 'Pluck_Plugin', 'plugin_activation' ) );
register_deactivation_hook( __FILE__, array( 'Pluck_Plugin', 'plugin_deactivation' ) );
?>
