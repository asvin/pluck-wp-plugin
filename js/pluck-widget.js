var app={

    initialize: function() {
        pluckAppProxy.registerActivityCallback("ReviewListRendered", this.onListRendered.bind(this));
        pluckAppProxy.registerActivityCallback("ReviewPreviewRendered", this.onPreviewRendered.bind(this));
        pluckAppProxy.registerActivityCallback("ReviewRollupRendered", this.onRollupRendered.bind(this));
        pluckAppProxy.registerActivityCallback("ReviewMostHelpfulRendered", this.onMostHelpfulRendered.bind(this));
    },

    onListRendered: function() {
        this.createStarRatings();
        this.createAttributeRatings();
        this.changeReviewActionIcons();
        this.moveReviewsCount();
    },

    onPreviewRendered: function() {
        this.createStarRatings();
        this.createAttributeRatings();
    },

    onRollupRendered: function() {
        this.createStarRatings();
        this.createStarDistributions();
    },

    onMostHelpfulRendered: function() {
        this.createStarRatings();
        this.createStarDistributions();
        this.moveMostHelpfulHeaders();
    },

    createStarRatings: function() {
        // this.bindUIElements();
        // console.log(this.ui.starContainer)

        dmJQuery(".pluck-review-starsOutput").each(function(index, element) {
            var numStars = 5;
            var output = "";
            var rating = dmJQuery(this).find("em").text();

            rating = (Math.floor(rating * 2) / 2).toFixed(1).replace(/\./g, '-');;

            for (var i = 0; i < numStars; i++) {
                    output += "<div class='pluck-review-star'></div>";
            };

            dmJQuery(this).html(output).addClass('rating-' + rating);
        });
    },

    createAttributeRatings: function() {
        // this.bindUIElements();

        dmJQuery(".pluck-review-attributesOutput").each(function(index, element) {
            var outputAttributes = "";
            var numberOfAttributes = dmJQuery(this).find("em").text();

            for (var i = 1; i <= 5; i++) {
                if (i <= numberOfAttributes) {
                    outputAttributes += "<div class='pluck-review-attribute pluck-review-attribute-active'></div>";
                } else {
                    outputAttributes += "<div class='pluck-review-attribute'></div>";
                }
            };

            dmJQuery(this).html(outputAttributes);

        });
    },

    createStarDistributions: function() {


        dmJQuery(".pluck-review-attributesOutput").each(function(index, element) {
            var percentAttribute = parseInt(dmJQuery(this).find("span em").css("left"), 10) + "%";
            var outputAttributes = "";
            var numberOfAttributes = dmJQuery(this).find("em").text();

            console.log(percentAttribute);

            for (var i = 1; i <= 5; i++) {
                outputAttributes += "<div class='pluck-review-attribute'></div>";
            };

            dmJQuery(this).html(outputAttributes);

            dmJQuery(this).css("background", "linear-gradient(to right, #F19848 " + percentAttribute + ", transparent " + percentAttribute + ")");

        });
    },

    changeReviewActionIcons: function() {
        dmJQuery(".pluck-review-full-review-comment-text").prepend("<i class='fa fa-comment'></i>");
        dmJQuery(".pluck-share-action-link").prepend("<i class='fa fa-share-alt'></i>");
        dmJQuery(".pluck-abuse-report-link").prepend("<i class='fa fa-exclamation-triangle'></i>");
        dmJQuery(".pluck-review-up-link").prepend("<i class='fa fa-thumbs-up'></i>");
        dmJQuery(".pluck-review-down-link").prepend("<i class='fa fa-thumbs-down'></i>");
    },

    moveReviewsCount: function() {
        dmJQuery(".pluck-review-full-header-headline").append(dmJQuery(".pluck-review-full-subheader-headline"));
    },

    moveMostHelpfulHeaders: function() {
        var outputHTML =    "<div class='pluck-review-full-header most-helpful-critical-header'>\
                                <p class='pluck-review-most-helpful-critical-headline'>Most Helpful Critical</p>\
                            </div>";

        dmJQuery(".pluck-review-most-helpful-critical-single-review-wrap").before(outputHTML);
    }
}

app.initialize();

